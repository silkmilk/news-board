import * as cheerio from 'cheerio';
import {Request, Response} from 'express';
import fetch from 'node-fetch';
import {URL, URLSearchParams} from 'url';

import {ArticleContent, ArticleListingInfo, GuardiansArticle, GuardiansSearch, LatestNewsContent} from '../../lib/interfaces';
import {GUARDIAN_API_KEY} from '../config';

/** The Guardians Search end point. */
const GUARDIAN_ENDPOINT = 'https://content.guardianapis.com/';

/**
 * A controller that handles fetching and serializing news articles from The
 * Guardians api.
 */
export class NewsController {
  /** Fetches and returns the latest news articles. */
  async getNews(req: Request, res: Response) {
    try {
      const url = new URL(GUARDIAN_ENDPOINT + '/search');
      const params = {
        'api-key': GUARDIAN_API_KEY,
        'page-size': req.query.pageSize || 20,
        'page': (req.query.page || 0) + 1,
        'show-fields': 'body',
      };

      url.search = new URLSearchParams(params).toString();

      const response = await fetch(url.toString());
      const data = await response.json();

      this.respondSuccess(res, this.serializeGuardianData(data.response));
    } catch (error) {
      this.respondError(res, error);
    }
  }

  /** Fetches and returns the latest 'Editors Picks' world news articles. */
  async getFeatured(_req: Request, res: Response) {
    try {
      const url = new URL(GUARDIAN_ENDPOINT + '/world');
      const params = {
        'api-key': GUARDIAN_API_KEY,
        'page-size': String(8),
        'show-fields': 'body',
        'show-editors-picks': 'true',
      };

      url.search = new URLSearchParams(params).toString();

      const response = await fetch(url.toString());
      const data = await response.json();

      // Filter out featured articles without images.
      const serialized = this.serializeGuardianData(data.response);
      serialized.articles =
          serialized.articles.filter(article => !!article.image);

      this.respondSuccess(res, serialized);
    } catch (error) {
      this.respondError(res, error);
    }
  }

  /** Fetches a single articles content. */
  async getArticle(req: Request, res: Response) {
    try {
      const url = new URL(GUARDIAN_ENDPOINT + '/' + req.query.articlePath);
      const params = {
        'api-key': GUARDIAN_API_KEY,
        'show-fields': 'body',
      };

      url.search = new URLSearchParams(params).toString();

      const response = await fetch(url.toString());
      const data = await response.json();

      this.respondSuccess(
          res, this.serializeArticleContent(data.response.content));
    } catch (error) {
      this.respondError(res, error);
    }
  }

  /** Serializes The Guardians response data. */
  serializeGuardianData(data: GuardiansSearch): LatestNewsContent {
    return {
      articles:
          data.results.map(article => this.serializeArticleListing(article)),
      count: data.total,
      pageSize: data.pageSize,
    }
  }

  /** Serializes single article listing objects. */
  serializeArticleListing(data: GuardiansArticle): ArticleListingInfo {
    const $ = cheerio.load(data.fields.body);

    return {
      datePublished: data.webPublicationDate,
      externalLink: data.webUrl,
      image: $('img').attr('src'),
      slug: data.id,
      title: data.webTitle,
    }
  }

  /** Serializes a single articles raw content. */
  serializeArticleContent(data: GuardiansArticle): ArticleContent {
    return {
      title: data.webTitle, body: data.fields.body,
    }
  }

  // tslint:disable-next-line:no-any
  respondSuccess(res: Response, data: any) {
    res.status(200).json({
      data,
      status: 200,
      message: 'Success',
    });
  }

  respondError(res: Response, error: Error) {
    // TODO: Show errors on development environments only
    console.log(error);
    res.status(500).json({
      status: 500,
      message: 'Oops, looks like something went wrong.',
    });
  }
}
