import * as express from 'express';
import {NewsController} from './controllers/news';

const router = express.Router();
const newsController = new NewsController();

router.get(
    '/news', async (req, res) => newsController.getNews(req, res));

router.get(
    '/featured', async (req, res) => newsController.getFeatured(req, res));

router.get(
    '/article', async (req, res) => newsController.getArticle(req, res));

export default router;
