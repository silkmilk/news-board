import * as cookieParser from 'cookie-parser';
import * as express from 'express';
import * as createError from 'http-errors';
import * as logger from 'morgan';
import * as path from 'path';

import router from './router';

const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// Add necessary CORS headers
// TODO: Ensure headers exist only on development environments
app.use((_req, res, next) => {
  res.header('Access-Control-Allow-Origin', 'http://localhost:4200');
  res.header(
      'Access-Control-Allow-Headers',
      'Origin, X-Requested-With, Content-Type, Accept');
  res.header('Access-Control-Allow-Methods', 'GET');
  next();
});

app.use('/api', router);

// Catch 404s and forward to error handler
app.use((_req, _res, next) => {
  next(createError(404));
});

/** TODO: Add error handling. */

export default app;
