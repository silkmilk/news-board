export declare interface BaseHttpResponse {
  message: string;
  status: number;
}

export declare interface LatestNewsResponse extends BaseHttpResponse {
  data: LatestNewsContent;
}

export declare interface FeaturedNewsResponse extends BaseHttpResponse {
  data: FeaturedNewsContent;
}

export declare interface ArticleResponse extends BaseHttpResponse {
  data: ArticleContent;
}

export declare interface LatestNewsContent {
  articles: ArticleListingInfo[];
  count: number;
  pageSize: number;
}

export declare interface FeaturedNewsContent {
  articles: ArticleListingInfo[];
}

export declare interface ArticleContent {
  body: string;
  title: string;
  image?: string;
}

export declare interface ArticleListingInfo {
  datePublished: string;
  externalLink: string;
  image?: string;
  slug: string;
  title: string;
}

export declare interface GuardiansSearch {
  pageSize: number;
  results: GuardiansArticle[];
  total: number;
}

export declare interface GuardiansArticle {
  fields: {[key: string]: string};
  id: string;
  webPublicationDate: string;
  webTitle: string;
  webUrl: string;
}
