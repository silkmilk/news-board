import {NgModule} from '@angular/core';

import {SafeHtmlPipe} from './safe_html_pipe';

@NgModule({
  declarations: [SafeHtmlPipe],
  exports: [SafeHtmlPipe],
})
export class PipesModule {
}
