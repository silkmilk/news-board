import {animate, AnimationTriggerMetadata, query, stagger, style, transition, trigger} from '@angular/animations';

/** Fades the content in when the component has been added to the DOM. */
export function fadeIn(
    name: string, startOffset = 0, endOffset = 0, duration = 300,
    delay = 0): AnimationTriggerMetadata {
  return trigger(name, [
    transition(
        ':enter',
        [
          style({opacity: 0, transform: `translateY(${startOffset}px)`}),
          animate(
              `${duration}ms ${delay}ms ease-out`,
              style({opacity: 1, transform: `translateY(${endOffset}px)`})),
        ]),
  ]);
}

/** Fades the content out when the component is being removed from the DOM. */
export function fadeOut(
    name: string, startOffset = 0, endOffset = 0, duration = 300,
    delay = 0): AnimationTriggerMetadata {
  return trigger(name, [
    transition(
        ':leave',
        [
          style({opacity: 1, transform: `translateY(${startOffset}px)`}),
          animate(
              `${duration}ms ${delay}ms ease-in`,
              style({opacity: 0, transform: `translateY(${endOffset}px)`})),
        ]),
  ]);
}

/**
 * Slides the content in when the component has been added to the DOM.
 * TODO: Fix production builds breaking when using string|number paramters.
 */
export function slideIn(
    name: string, startOffset = '0', endOffset = '0', duration = 300,
    delay = 0): AnimationTriggerMetadata {
  return trigger(name, [
    transition(
        ':enter',
        [
          style({transform: `translateX(${startOffset})`}),
          animate(
              `${duration}ms ${delay}ms ease-out`,
              style({transform: `translateX(${endOffset})`})),
        ]),
  ]);
}

/** Slides the content in when the component has been removed to the DOM. */
export function slideOut(
    name: string, startOffset = '0', endOffset = '0', duration = 300,
    delay = 0): AnimationTriggerMetadata {
  return trigger(name, [
    transition(
        ':leave',
        [
          style({transform: `translateX(${startOffset})`}),
          animate(
              `${duration}ms ${delay}ms ease-out`,
              style({transform: `translateX(${endOffset})`})),
        ]),
  ]);
}

export function staggerFadeIn(
    name: string, duration = 300, staggerAmount = 100, startOffset = 0) {
  return trigger(
      name, [transition(':enter', [
        query(
            ':enter',
            [
              style({opacity: 0, transform: `translateY(${startOffset}px)`}),
              stagger(
                  staggerAmount,
                  [
                    animate(
                        `${duration}ms ease`,
                        style({opacity: 1, transform: 'translateY(0)'})),
                  ])
            ],
            {optional: true})
      ])]);
}
