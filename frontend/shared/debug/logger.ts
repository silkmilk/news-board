/**
 * Safely log to the console on development only environments.
 */
import {production} from '../../environments/environment';

// tslint:disable: no-console no-any
export function info(...messages: any[]) {
  if (!production) {
    console.info(...messages);
  }
}

export function warn(...messages: any[]) {
  if (!production) {
    console.warn(...messages);
  }
}

export function error(...messages: any[]) {
  if (!production) {
    console.error(...messages);
  }
}
// tslint:enable: no-console no-any
