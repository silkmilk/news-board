import {HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';

import {MetaTagsService} from './meta_tags';
import {NewsService} from './news';
import {ScrollService} from './scroll';

@NgModule({
  imports: [HttpClientModule],
  providers: [MetaTagsService, NewsService, ScrollService],
})
export class ServicesModule {
}
