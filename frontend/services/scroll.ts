import {Injectable} from '@angular/core';
import {animationFrameScheduler, fromEvent, Observable} from 'rxjs';
import {map, startWith, throttleTime} from 'rxjs/operators';

@Injectable()
export class ScrollService {
  constructor() {}

  /** Returns an observable that updates when the user scrolls the page. */
  getScrollPosition(): Observable<number> {
    return fromEvent(window, 'scroll')
        .pipe(
            throttleTime(0, animationFrameScheduler),
            startWith(this.getCurrentScrollPosition()),
            map(() => this.getCurrentScrollPosition()));
  }

  private getCurrentScrollPosition(): number {
    return window.pageYOffset || document.documentElement.scrollTop;
  }
}
