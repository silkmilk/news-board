import {Injectable} from '@angular/core';
import {Meta, MetaDefinition} from '@angular/platform-browser';

@Injectable()
export class MetaTagsService {
  private hasAddedInitialTags = false;

  constructor(private readonly meta: Meta) {}

  /** Sets all HTML meta tags. */
  setTags(title: string, description: string, image?: string) {
    const definitions = this.getDefinitions(title, description, image);

    if (this.hasAddedInitialTags) {
      this.updateTags(definitions);
    } else {
      this.hasAddedInitialTags = true;
      this.addTags(definitions);
    }
  }

  /** Updates existing meta tags. */
  private updateTags(definitions: MetaDefinition[]) {
    definitions.forEach(definition => this.meta.updateTag(definition));
  }

  /** Adds new meta tags to the document. */
  private addTags(definitions: MetaDefinition[]) {
    definitions.forEach(definition => this.meta.addTag(definition));
  }

  /** Returns a list of all meta tag definitions. */
  private getDefinitions(title: string, description: string, image = ''):
      MetaDefinition[] {
    const defaultTags = [
      {name: 'title', content: title},
      {name: 'description', content: description},
      {name: 'image', content: image},
    ];

    const ogTags = [
      {name: 'og:title', content: title},
      {name: 'og:description', content: description},
      {name: 'og:image', content: image},
    ];

    const twitterTags = [
      {name: 'twitter:card', content: 'summary'},
      {name: 'twitter:title', content: title},
      {name: 'twitter:description', content: description},
      {name: 'twitter:image', content: image},
    ];

    return [...defaultTags, ...ogTags, ...twitterTags];
  }
}
