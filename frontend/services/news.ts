import {HttpClient, HttpParams} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

import {ArticleContent, ArticleResponse, FeaturedNewsContent, FeaturedNewsResponse, LatestNewsContent, LatestNewsResponse} from '../../lib/interfaces';
import {apiSource} from '../environments/environment';

@Injectable()
export class NewsService {
  constructor(private readonly http: HttpClient) {}

  fetchLatestNews(pageSize: number, page: number):
      Observable<LatestNewsContent> {
    const params = new HttpParams({
      fromObject: {
        pageSize: String(pageSize),
        page: String(page),
      }
    });

    return this.http.get<LatestNewsResponse>(`${apiSource}news`, {params})
        .pipe(map(response => response.data));
  }

  fetchFeaturedNews(): Observable<FeaturedNewsContent> {
    return this.http.get<FeaturedNewsResponse>(`${apiSource}featured`)
        .pipe(map(response => response.data));
  }

  fetchArticle(slug: string): Observable<ArticleContent> {
    const params = new HttpParams({
      fromObject: {
        articlePath: String(slug),
      }
    });

    return this.http.get<ArticleResponse>(`${apiSource}article`, {params})
        .pipe(map(response => response.data));
  }
}
