import {Component, Input, OnDestroy} from '@angular/core';
import {interval, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {ArticleListingInfo} from '../../../lib/interfaces';
import {NewsService} from '../../services/news';
import {fadeIn, slideIn, slideOut} from '../../shared/animations';

/** The time in milliseconds to show each slide. */
export const SLIDE_DISPLAY_TIME = 8000;

@Component({
  selector: 'nw-featured-news',
  templateUrl: './featured_news.html',
  styleUrls: ['./featured_news.scss'],
  animations: [
    slideIn('cardInAnimation', '100%', '0', 600, 0),
    slideOut('cardOutAnimation', '0', '-100%', 600, 0),
    fadeIn('fadeInAll', 0, 0, 0, 400),
  ]
})
export class FeaturedNewsComponent implements OnDestroy {
  @Input() cards: ArticleListingInfo[] = [];

  /**
   * Counts the number of total slide changes, used for calculating which slide
   * to show.
   */
  private slideCount = 0;

  private destroy: Subject<boolean> = new Subject<boolean>();

  constructor(private readonly newsService: NewsService) {
    this.newsService.fetchFeaturedNews().subscribe(data => {
      this.cards = data.articles;
      this.startLoopingSlides();
    });
  }

  ngOnDestroy() {
    this.destroy.next(true);
    this.destroy.unsubscribe();
  }

  /** Returns the index for the currently showing slide. */
  getCurrentSlide(): number {
    if (!this.slideCount || !this.cards || !this.cards.length) {
      return 0;
    }

    return this.slideCount % this.cards.length;
  }

  /** Begins the sliding loop. */
  private startLoopingSlides() {
    interval(SLIDE_DISPLAY_TIME)
        .pipe(takeUntil(this.destroy))
        .subscribe((val) => {
          this.slideCount++;
        })
  }
}
