import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';

import {LatestNewsResponse} from '../../../lib/interfaces';
import {NewsService} from '../../services/news';
import {ServicesModule} from '../../services/services_module';
import {NewsBoardModule} from '../news_board_module';

import {NewsCardListComponent} from './news_card_list';

export const MOCK_ARTICLES_LENGTH = 10;

export const MOCK_ARTICLES = Array.apply(null, {length: MOCK_ARTICLES_LENGTH})
                                 .map(() => ({
                                        datePublised: '2018-09-23T07:00:00Z',
                                        externalLink: '',
                                        slug: '',
                                        title: '',
                                      }));

export const MOCK_NEWS_RESPONSE: LatestNewsResponse = {
  data: {
    articles: MOCK_ARTICLES,
    count: MOCK_ARTICLES_LENGTH,
    pageSize: MOCK_ARTICLES_LENGTH,
  },
  status: 200,
  message: '',
};

describe('NewsCardListComponent', () => {
  let component: NewsCardListComponent;
  let fixture: ComponentFixture<NewsCardListComponent>;
  let element: HTMLElement;
  let httpMock: HttpTestingController;
  let newsService: NewsService;

  beforeEach(async () => {
    TestBed
        .configureTestingModule({
          imports: [
            HttpClientTestingModule,
            NewsBoardModule,
            RouterTestingModule.withRoutes([]),
            ServicesModule,
          ],
        })
        .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsCardListComponent);
    component = fixture.debugElement.componentInstance;
    element = fixture.debugElement.nativeElement;
    newsService = TestBed.get(NewsService);
    httpMock = TestBed.get(HttpTestingController);
    fixture.detectChanges();
  });

  it('should create the NewsCardList', () => {
    expect(component).toBeTruthy();
  });

  it(`should contain a list of NewsCards`, () => {
    const contentReq =
        httpMock.expectOne(req => req.url === 'http://localhost:3000/api/news');
    contentReq.flush(MOCK_NEWS_RESPONSE);
    fixture.detectChanges();

    const list = element.querySelector('ul');

    if (list) {
      const cards = list.querySelectorAll('nw-news-card') || [];
      expect(cards.length).toEqual(MOCK_ARTICLES_LENGTH);
    }
  });
});
