import {Component, OnDestroy} from '@angular/core';
import {fromEvent, Subject, timer} from 'rxjs';
import {debounce, takeUntil} from 'rxjs/operators';

import {ArticleListingInfo} from '../../../lib/interfaces';
import {NewsService} from '../../services/news';
import {ScrollService} from '../../services/scroll';
import {staggerFadeIn} from '../../shared/animations';

/** The rendered height of a single news card. */
const CARD_HEIGHT = 320;

/** The number of rows */
const PAGE_ROW_OVERLAP = 1;

/** Pixels before scrolling to bottom of document to trigger a page load. */
const LOAD_PAGE_OFFSET = 600;

@Component({
  selector: 'nw-news-card-list',
  templateUrl: './news_card_list.html',
  styleUrls: ['./news_card_list.scss'],
  animations: [
    staggerFadeIn('staggerCards', 400, 50, 30),
  ],
})
export class NewsCardListComponent implements OnDestroy {
  cards: ArticleListingInfo[] = [];

  private pageSize: number;
  private currentPage = 0;
  private isLoadingPage = true;

  private destroy: Subject<boolean> = new Subject<boolean>();

  constructor(
      private readonly newsService: NewsService,
      private readonly scrollService: ScrollService) {
    this.setPageSize();

    this.newsService.fetchLatestNews(this.pageSize, 0).subscribe(data => {
      this.cards = data.articles;
      this.isLoadingPage = false;
    });

    fromEvent(window, 'resize')
        .pipe(debounce(() => timer(80)))
        .pipe(takeUntil(this.destroy))
        .subscribe(() => this.setPageSize());

    this.scrollService.getScrollPosition()
        .pipe(takeUntil(this.destroy))
        .subscribe(position => this.loadNextPage(position))
  }

  ngOnDestroy() {
    this.destroy.next(true);
    this.destroy.unsubscribe();
  }

  private loadNextPage(position: number) {
    const loadPosition =
        document.body.scrollHeight - window.innerHeight - LOAD_PAGE_OFFSET;
    if (position > loadPosition && !this.isLoadingPage) {
      this.isLoadingPage = true;

      this.newsService.fetchLatestNews(this.pageSize, ++this.currentPage)
          .subscribe(data => {
            this.cards.push(...data.articles);
            this.isLoadingPage = false;
          });
    }
  }

  private setPageSize() {
    const columns = this.getColumnsPerRow();
    const rowsPerWindow = Math.ceil(window.innerHeight / CARD_HEIGHT);
    this.pageSize = rowsPerWindow * columns + (columns * PAGE_ROW_OVERLAP);
  }

  /** Returns the columns per row based on the CSS breakpoints. */
  private getColumnsPerRow(): number {
    const windowWidth = window.innerWidth;

    if (windowWidth < 500) {
      return 1;
    } else if (windowWidth < 768) {
      return 2;
    } else if (windowWidth < 1024) {
      return 3;
    } else if (windowWidth < 1280) {
      return 3;
    } else if (windowWidth < 1440) {
      return 4;
    } else {
      return 5;
    }
  }
}
