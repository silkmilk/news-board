import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {RouterModule} from '@angular/router';

import {ServicesModule} from '../services/services_module';

import {FeaturedNewsComponent} from './featured_news/featured_news';
import {NewsBoardComponent} from './news_board/news_board';
import {NewsCardComponent} from './news_card/news_card';
import {NewsCardListComponent} from './news_card_list/news_card_list';

@NgModule({
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    CommonModule,
    RouterModule,
    ServicesModule,
  ],
  declarations: [
    FeaturedNewsComponent,
    NewsBoardComponent,
    NewsCardComponent,
    NewsCardListComponent,
  ],
  exports: [NewsBoardComponent],
})
export class NewsBoardModule {
}
