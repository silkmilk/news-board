import {Component} from '@angular/core';
import {MetaTagsService} from '../../services/meta_tags';

@Component({
  selector: 'nw-news-board',
  templateUrl: './news_board.html',
  styleUrls: ['./news_board.scss'],
})
export class NewsBoardComponent {
  constructor(private readonly metaTagsService: MetaTagsService) {
    this.metaTagsService.setTags('News Board', 'The Guardians latest news.');
  }
}
