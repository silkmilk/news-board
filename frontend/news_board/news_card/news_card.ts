import {Component, HostBinding, Input} from '@angular/core';
import {ArticleListingInfo} from '../../../lib/interfaces';

@Component({
  selector: 'nw-news-card',
  templateUrl: './news_card.html',
  styleUrls: ['./news_card.scss'],
})
export class NewsCardComponent {
  @Input() content: ArticleListingInfo;
  @Input() index: number;
  @HostBinding('class.is-featured') @Input() isFeatured = false;
}
