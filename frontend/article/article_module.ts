import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {RouterModule} from '@angular/router';

import {ServicesModule} from '../services/services_module';
import {PipesModule} from '../shared/pipes/pipes_module';

import {ArticleComponent} from './article';

@NgModule({
  imports: [
    BrowserModule,
    CommonModule,
    BrowserAnimationsModule,
    RouterModule,
    ServicesModule,
    PipesModule,
  ],
  declarations: [ArticleComponent],
  exports: [ArticleComponent],
})
export class ArticleModule {
}
