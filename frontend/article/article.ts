import {Component, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Subject} from 'rxjs';
import {catchError, map, mergeMap, takeUntil} from 'rxjs/operators';

import {ArticleContent} from '../../lib/interfaces';
import {MetaTagsService} from '../services/meta_tags';
import {NewsService} from '../services/news';
import {fadeIn} from '../shared/animations';

@Component({
  selector: 'nw-article',
  templateUrl: './article.html',
  styleUrls: ['./article.scss'],
  animations: [
    fadeIn('backButtonIn', 0, 0, 650, 300),
    fadeIn('titleIn', 50, 0, 600, 0),
    fadeIn('contentIn', 50, 0, 600, 150),
  ]
})
export class ArticleComponent implements OnDestroy {
  content: ArticleContent;

  private slug: string|null = null;

  private destroy: Subject<boolean> = new Subject<boolean>();

  constructor(
      private readonly route: ActivatedRoute,
      private readonly newsService: NewsService,
      private readonly metaTagsService: MetaTagsService) {
    this.route.paramMap.pipe(takeUntil(this.destroy))
        .pipe(map(params => this.slug = params.get('article')))
        .pipe(mergeMap(() => this.newsService.fetchArticle(this.slug || '')))
        .pipe(catchError((_error, caught) => {
          // TODO: Route to error page.
          return caught;
        }))
        .subscribe(content => {
          // TODO: Extract summary.
          this.metaTagsService.setTags(content.title, '', content.image);
          this.content = content;
        });
  }

  ngOnDestroy() {
    this.destroy.next(true);
    this.destroy.unsubscribe();
  }
}
