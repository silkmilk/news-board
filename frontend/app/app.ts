import {Component} from '@angular/core';

@Component({
  selector: 'nw-app-root',
  templateUrl: './app.html',
  styleUrls: ['./app.scss'],
})
export class AppComponent {
  title = 'news-board';
}
