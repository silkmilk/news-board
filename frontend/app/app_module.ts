import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {InlineSVGModule} from 'ng-inline-svg';

import {ArticleModule} from '../article/article_module';
import {NewsBoardModule} from '../news_board/news_board_module';

import {AppComponent} from './app';
import {AppRouting} from './app_routing';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    CommonModule,
    InlineSVGModule.forRoot(),
    AppRouting,
    NewsBoardModule,
    ArticleModule,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
}
