import {APP_BASE_HREF, CommonModule, Location} from '@angular/common';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {inject} from '@angular/core';
import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {Router, UrlSegment} from '@angular/router';
import {RouterTestingModule} from '@angular/router/testing'

import {ServicesModule} from '../services/services_module';

import {AppComponent} from './app';
import {AppModule} from './app_module';
import {articleMatcher, routes} from './app_routing';

describe('AppComponent', () => {
  let fixture: ComponentFixture<AppComponent>;
  let component: AppComponent;
  let element: HTMLElement;
  let location: Location;
  let router: Router;

  beforeEach(async(() => {
    TestBed
        .configureTestingModule({
          declarations: [],
          imports: [
            AppModule,
            CommonModule,
            RouterTestingModule.withRoutes(routes),
            NoopAnimationsModule,
            HttpClientTestingModule,
          ],
          providers: [
            Location,
            ServicesModule,
            {provide: APP_BASE_HREF, useValue: '/'},
          ],
        })
        .compileComponents();

    router = TestBed.get(Router);
    location = TestBed.get(Location);
  }));

  beforeEach(async(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.debugElement.componentInstance;
    element = fixture.debugElement.nativeElement;
    router.initialNavigation();
    fixture.detectChanges();
  }));

  it('should create the app', () => {
    expect(component).toBeTruthy();
  });

  it(`should show the news board on the home page`, () => {
    expect(element.querySelector('nw-news-board')).toBeTruthy();
  });

  it(`should show an article on and article page`, fakeAsync(() => {
       router.navigate(['article/testing/slug']);
       tick();
       expect(element.querySelector('nw-article')).toBeTruthy();
     }));
});


describe('UrlMatcher', () => {
  let router: Router;

  beforeEach(() => {
    TestBed.configureTestingModule({imports: [RouterTestingModule]})
        .compileComponents();
    router = TestBed.get(Router);
  });

  it('should ignore none article paths', () => {
    const home: UrlSegment[] = [];
    const one = getSegmentsForPath('/articles');
    const two = getSegmentsForPath('/articl');

    expect(articleMatcher(home)).toBeNull();
    expect(articleMatcher(one)).toBeNull();
    expect(articleMatcher(two)).toBeNull();
  });

  it('should ignore articles without slugs', () => {
    const article = getSegmentsForPath('/article');
    expect(articleMatcher(article)).toBeNull();
  });

  it('should match anything after article', () => {
    const one = getSegmentsForPath('/article/one');
    const two = getSegmentsForPath('/article/one/two');
    const three = getSegmentsForPath('/article/one/two/three');
    const query = getSegmentsForPath('/article/one?q=query');
    expect(articleMatcher(one)).toBeTruthy();
    expect(articleMatcher(two)).toBeTruthy();
    expect(articleMatcher(three)).toBeTruthy();
    expect(articleMatcher(query)).toBeTruthy();
  });

  it('should return the full article slug containing forward slashes', () => {
    const article = getSegmentsForPath('/article/category/date/slug');
    const result = articleMatcher(article);
    if (result.posParams) {
      const slug = decodeURIComponent(result.posParams.article.toString());
      expect(slug).toEqual('category/date/slug');
    } else {
      fail(`'result.posParams' does not exist.`);
    }
  });

  function getSegmentsForPath(path: string) {
    return router.parseUrl(path).root.children.primary.segments;
  }
});
