import {NgModule} from '@angular/core';
import {RouterModule, Routes, UrlMatchResult, UrlSegment} from '@angular/router';

import {ArticleComponent} from '../article/article';
import {NewsBoardComponent} from '../news_board/news_board/news_board';

export function articleMatcher(url: UrlSegment[]): UrlMatchResult {
  const isArticle = url.length > 1 && url[0].path.startsWith('article');

  if (isArticle) {
    const slug = url.slice(2).reduce(
        (accum, curr) => `${accum.toString()}/${curr.toString()}`,
        url[1].toString());
    const param = new UrlSegment(slug, {});

    return {
      consumed: url,
      posParams: {
        'article': param,
      }
    };
  } else {
    // Ignore an Angular bug that prevents returning null.
    // https://github.com/angular/angular/pull/24464
    // @ts-ignore
    return null;
  }
}

export const routes: Routes = [
  {
    matcher: articleMatcher,
    pathMatch: 'full',
    component: ArticleComponent,
  },
  {
    path: '',
    pathMatch: 'full',
    component: NewsBoardComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRouting {
}
