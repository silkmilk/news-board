# news-board

A news board taking advantage of The Guardians api to render the latest world articles in an updated, more easily digestible, format.


## Tech stack

* Angular 6
* Node (TypeScript)
* Express


## Prerequisites

* [Node](https://nodejs.org/en/) / [npm](https://www.npmjs.com/)
    * Recommend installing with [nvm](https://github.com/creationix/nvm)

## Setup

* $ nvm install
* $ npm install


## Running

Development mode:

$ npm run dev
